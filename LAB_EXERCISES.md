## Lab Exercise ##

### Part 1. Warming up to xml and connecting activities with views.###
Format the json string returned so that it appears nicely on the screen in the following format.

**Name**

Luke Skywalker

**Movies Appeared In**

Star Wars 1

Star Wars 2

...

### Part 2. Using APIs in activities. ###
Right now, you can only search for people.
Read up on the documentation at https://swapi.co/documentation#search
to enable searching for anything.

Hint: go to https://swapi.co/api/?search to see which objects are searchable.