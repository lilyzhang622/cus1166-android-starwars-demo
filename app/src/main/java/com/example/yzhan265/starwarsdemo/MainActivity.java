package com.example.yzhan265.starwarsdemo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

// all activities inherit from AppCompat Activity
public class MainActivity extends AppCompatActivity {

    // override the different methods called throughout the activity lifecycle
    // to define your own code
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // define which layout file this activity will use.
        setContentView(R.layout.activity_main);

        // get a reference to the view
        TextView welcomeText = (TextView) findViewById(R.id.welcome_textview);

        // attach an onClick event listener to the button.
        welcomeText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextActivity(v);
            }
        });

    }

    protected void nextActivity(View view){
        // define an intent to start a new activity
        Intent intent = new Intent(this, SearchActivity.class);

        // start the intent and launch the next activity
        startActivity(intent);
    }
}
