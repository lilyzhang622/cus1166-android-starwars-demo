package com.example.yzhan265.starwarsdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;


public class SearchActivity extends AppCompatActivity {
    Button searchButton;
    EditText searchText;
    TextView resultsText;

    @Override
    protected void onCreate(Bundle savedInstanceBundle){
        Log.d("SearchActivity","onCreate");
        super.onCreate(savedInstanceBundle);
        setContentView(R.layout.activity_search);

        searchButton = (Button) findViewById(R.id.search_btn);
        searchText = (EditText) findViewById(R.id.search_text);
        resultsText = (TextView) findViewById(R.id.results_text);

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // log your code to debug it equivalent to system.out.print();
                Log.d("search button onclick", "button clicked");

                // retrieve the text in the edittext view (in the search bar)
                String term = searchText.getText().toString();
                Log.d("search for term", term);
                resultsText.setText("Searching for:"+term);

                // search for that term
                searchForTerm(term);
            }
        });

    }


    protected void searchForTerm(String term) {
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "https://swapi.co/api/people/?search="+term;
        Log.d("search for term","searching for term");

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("stringRequest", response.toString());
                        resultsText.setText("Response: " + response.toString());
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        resultsText.setText("That didn't work!");
                    }
                });
        queue.add(jsonObjectRequest);

    }
}
