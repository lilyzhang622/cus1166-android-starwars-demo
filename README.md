### Star Wars Android App Demo ###

1. Download Android Studio at: https://developer.android.com/studio/index.html

2. 
	```
	# clone this repository
	git clone https://lilyzhang622@bitbucket.org/lilyzhang622/cus1166-android-starwars-demo.git
	```

3. Go to Android Studio and select open an existing Android Studio Project and navigate to the folder you cloned the repository into.

### Quick Links ###

* [Getting Started](https://bitbucket.org/lilyzhang622/cus1166-android-starwars-demo/src/7946d943240382bb899acbd8dbcf9abc3303da0b/GETTING_STARTED.pptx?at=master&fileviewer=file-view-default)
* [Presentation](https://bitbucket.org/lilyzhang622/cus1166-android-starwars-demo/src/7946d943240382bb899acbd8dbcf9abc3303da0b/PRESENTATION_POWERPOINT.pptx?at=master&fileviewer=file-view-default)
* [Lab Exercise](https://bitbucket.org/lilyzhang622/cus1166-android-starwars-demo/src/7946d943240382bb899acbd8dbcf9abc3303da0b/LAB_EXERCISES.md?at=master&fileviewer=file-view-default)